/*
 * client/js/views/home/content.js
 */

/* global define */

define([
  'backbone',
  'hbs!templates/home/content'
], function (Backbone, contentTpl) {
  'use strict';

  return Backbone.Marionette.CompositeView.extend({
    template: contentTpl
  });
});
