/*
 * client/js/collections/top.js
 */

/* global define */

define([
  'backbone',
  'models/listitem'
], function (Backbone, ListItem) {
  'use strict';

  return Backbone.Collection.extend({
    model: ListItem,
    url: '/api/content'
  });
});
