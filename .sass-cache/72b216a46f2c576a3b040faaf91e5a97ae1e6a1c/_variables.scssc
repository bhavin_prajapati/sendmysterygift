3.2.10 (Media Mark)
44d673ff77e8013bcef9690da6399ad8aa8cb308
o:Sass::Tree::RootNode
:@template"7)//
// Variables
// --------------------------------------------------


// Global values
// --------------------------------------------------


// Grays
// -------------------------
$black:                 #000 !default;
$grayDarker:            #222 !default;
$grayDark:              #333 !default;
$gray:                  #555 !default;
$grayLight:             #999 !default;
$grayLighter:           #eee !default;
$white:                 #fff !default;


// Accent colors
// -------------------------
$blue:                  #049cdb !default;
$blueDark:              #0064cd !default;
$green:                 #46a546 !default;
$red:                   #9d261d !default;
$yellow:                #ffc40d !default;
$orange:                #f89406 !default;
$pink:                  #c3325f !default;
$purple:                #7a43b6 !default;


// Scaffolding
// -------------------------
$bodyBackground:        $white !default;
$textColor:             $grayDark !default;


// Links
// -------------------------
$linkColor:             #08c !default;
$linkColorHover:        darken($linkColor, 15%) !default;


// Typography
// -------------------------
$sansFontFamily:        "Helvetica Neue", Helvetica, Arial, sans-serif !default;
$serifFontFamily:       Georgia, "Times New Roman", Times, serif !default;
$monoFontFamily:        Monaco, Menlo, Consolas, "Courier New", monospace !default;

$baseFontSize:          14px !default;
$baseFontFamily:        $sansFontFamily !default;
$baseLineHeight:        20px !default;
$altFontFamily:         $serifFontFamily !default;

$headingsFontFamily:    inherit !default; // empty to use BS default, $baseFontFamily
$headingsFontWeight:    bold !default;    // instead of browser default, bold
$headingsColor:         inherit !default; // empty to use BS default, $textColor


// Component sizing
// -------------------------
// Based on 14px font-size and 20px line-height

$fontSizeLarge:         $baseFontSize * 1.25 !default; // ~18px
$fontSizeSmall:         $baseFontSize * 0.85 !default; // ~12px
$fontSizeMini:          $baseFontSize * 0.75 !default; // ~11px

$paddingLarge:          11px 19px !default; // 44px
$paddingSmall:          2px 10px !default;  // 26px
$paddingMini:           0px 6px !default;   // 22px

$baseBorderRadius:      4px !default;
$borderRadiusLarge:     6px !default;
$borderRadiusSmall:     3px !default;


// Tables
// -------------------------
$tableBackground:                   transparent !default; // overall background-color
$tableBackgroundAccent:             #f9f9f9 !default; // for striping
$tableBackgroundHover:              #f5f5f5 !default; // for hover
$tableBorder:                       #ddd !default; // table and cell border

// Buttons
// -------------------------
$btnBackground:                     $white !default;
$btnBackgroundHighlight:            darken($white, 10%) !default;
$btnBorder:                         #ccc !default;

$btnPrimaryBackground:              $linkColor !default;
$btnPrimaryBackgroundHighlight:     adjust-hue($btnPrimaryBackground, 20%) !default;

$btnInfoBackground:                 #5bc0de !default;
$btnInfoBackgroundHighlight:        #2f96b4 !default;

$btnSuccessBackground:              #62c462 !default;
$btnSuccessBackgroundHighlight:     #51a351 !default;

$btnWarningBackground:              lighten($orange, 15%) !default;
$btnWarningBackgroundHighlight:     $orange !default;

$btnDangerBackground:               #ee5f5b !default;
$btnDangerBackgroundHighlight:      #bd362f !default;

$btnInverseBackground:              #444 !default;
$btnInverseBackgroundHighlight:     $grayDarker !default;


// Forms
// -------------------------
$inputBackground:               $white !default;
$inputBorder:                   #ccc !default;
$inputBorderRadius:             $baseBorderRadius !default;
$inputDisabledBackground:       $grayLighter !default;
$formActionsBackground:         #f5f5f5 !default;
$inputHeight:                   $baseLineHeight + 10px; // base line-height + 8px vertical padding + 2px top/bottom border


// Dropdowns
// -------------------------
$dropdownBackground:            $white !default;
$dropdownBorder:                rgba(0,0,0,.2) !default;
$dropdownDividerTop:            #e5e5e5 !default;
$dropdownDividerBottom:         $white !default;

$dropdownLinkColor:             $grayDark !default;
$dropdownLinkColorHover:        $white !default;
$dropdownLinkColorActive:       $white !default;

$dropdownLinkBackgroundActive:  $linkColor !default;
$dropdownLinkBackgroundHover:   $dropdownLinkBackgroundActive !default;



// COMPONENT VARIABLES
// --------------------------------------------------


// Z-index master list
// -------------------------
// Used for a bird's eye view of components dependent on the z-axis
// Try to avoid customizing these :)
$zindexDropdown:          1000 !default;
$zindexPopover:           1010 !default;
$zindexTooltip:           1030 !default;
$zindexFixedNavbar:       1030 !default;
$zindexModalBackdrop:     1040 !default;
$zindexModal:             1050 !default;


// Sprite icons path
// -------------------------
$iconSpritePath:          "../img/glyphicons-halflings.png" !default;
$iconWhiteSpritePath:     "../img/glyphicons-halflings-white.png" !default;


// Input placeholder text color
// -------------------------
$placeholderText:         $grayLight !default;


// Hr border color
// -------------------------
$hrBorder:                $grayLighter !default;


// Horizontal forms & lists
// -------------------------
$horizontalComponentOffset:       180px !default;


// Wells
// -------------------------
$wellBackground:                  #f5f5f5 !default;


// Navbar
// -------------------------
$navbarCollapseWidth:             979px !default;
$navbarCollapseDesktopWidth:      $navbarCollapseWidth + 1;

$navbarHeight:                    40px !default;
$navbarBackgroundHighlight:       #ffffff !default;
$navbarBackground:                darken($navbarBackgroundHighlight, 5%) !default;
$navbarBorder:                    darken($navbarBackground, 12%) !default;

$navbarText:                      #777 !default;
$navbarLinkColor:                 #777 !default;
$navbarLinkColorHover:            $grayDark !default;
$navbarLinkColorActive:           $gray !default;
$navbarLinkBackgroundHover:       transparent !default;
$navbarLinkBackgroundActive:      darken($navbarBackground, 5%) !default;

$navbarBrandColor:                $navbarLinkColor !default;

// Inverted navbar
$navbarInverseBackground:                #111111 !default;
$navbarInverseBackgroundHighlight:       #222222 !default;
$navbarInverseBorder:                    #252525 !default;

$navbarInverseText:                      $grayLight !default;
$navbarInverseLinkColor:                 $grayLight !default;
$navbarInverseLinkColorHover:            $white !default;
$navbarInverseLinkColorActive:           $navbarInverseLinkColorHover !default;
$navbarInverseLinkBackgroundHover:       transparent !default;
$navbarInverseLinkBackgroundActive:      $navbarInverseBackground !default;

$navbarInverseSearchBackground:          lighten($navbarInverseBackground, 25%) !default;
$navbarInverseSearchBackgroundFocus:     $white !default;
$navbarInverseSearchBorder:              $navbarInverseBackground !default;
$navbarInverseSearchPlaceholderColor:    #ccc !default;

$navbarInverseBrandColor:                $navbarInverseLinkColor !default;


// Pagination
// -------------------------
$paginationBackground:                #fff !default;
$paginationBorder:                    #ddd !default;
$paginationActiveBackground:          #f5f5f5 !default;


// Hero unit
// -------------------------
$heroUnitBackground:              $grayLighter !default;
$heroUnitHeadingColor:            inherit !default;
$heroUnitLeadColor:               inherit !default;


// Form states and alerts
// -------------------------
$warningText:             #c09853 !default;
$warningBackground:       #fcf8e3 !default;
$warningBorder:           darken(adjust-hue($warningBackground, -10), 3%) !default;

$errorText:               #b94a48 !default;
$errorBackground:         #f2dede !default;
$errorBorder:             darken(adjust-hue($errorBackground, -10), 3%) !default;

$successText:             #468847 !default;
$successBackground:       #dff0d8 !default;
$successBorder:           darken(adjust-hue($successBackground, -10), 5%) !default;

$infoText:                #3a87ad !default;
$infoBackground:          #d9edf7 !default;
$infoBorder:              darken(adjust-hue($infoBackground, -10), 7%) !default;


// Tooltips and popovers
// -------------------------
$tooltipColor:            #fff !default;
$tooltipBackground:       #000 !default;
$tooltipArrowWidth:       5px !default;
$tooltipArrowColor:       $tooltipBackground !default;

$popoverBackground:       #fff !default;
$popoverArrowWidth:       10px !default;
$popoverArrowColor:       #fff !default;
$popoverTitleBackground:  darken($popoverBackground, 3%) !default;

// Special enhancement for popovers
$popoverArrowOuterWidth:  $popoverArrowWidth + 1 !default;
$popoverArrowOuterColor:  rgba(0,0,0,.25) !default;



// GRID
// --------------------------------------------------


// Default 940px grid
// -------------------------
$gridColumns:             12 !default;
$gridColumnWidth:         60px !default;
$gridGutterWidth:         20px !default;
$gridRowWidth:            ($gridColumns * $gridColumnWidth) + ($gridGutterWidth * ($gridColumns - 1)) !default;

// 1200px min
$gridColumnWidth1200:     70px !default;
$gridGutterWidth1200:     30px !default;
$gridRowWidth1200:        ($gridColumns * $gridColumnWidth1200) + ($gridGutterWidth1200 * ($gridColumns - 1)) !default;

// 768px-979px
$gridColumnWidth768:      42px !default;
$gridGutterWidth768:      20px !default;
$gridRowWidth768:         ($gridColumns * $gridColumnWidth768) + ($gridGutterWidth768 * ($gridColumns - 1)) !default;


// Fluid grid
// -------------------------
$fluidGridColumnWidth:    percentage($gridColumnWidth/$gridRowWidth) !default;
$fluidGridGutterWidth:    percentage($gridGutterWidth/$gridRowWidth) !default;

// 1200px min
$fluidGridColumnWidth1200:     percentage($gridColumnWidth1200/$gridRowWidth1200) !default;
$fluidGridGutterWidth1200:     percentage($gridGutterWidth1200/$gridRowWidth1200) !default;

// 768px-979px
$fluidGridColumnWidth768:      percentage($gridColumnWidth768/$gridRowWidth768) !default;
$fluidGridGutterWidth768:      percentage($gridGutterWidth768/$gridRowWidth768) !default;
:@has_childrenT:@options{ :@children[�o:Sass::Tree::CommentNode
:
@type:silent:@value["M/*
 * Variables
 * -------------------------------------------------- */;@;	[ :
@lineio;

;;;["N/* Global values
 * -------------------------------------------------- */;@;	[ ;io;

;;;["-/* Grays
 * ------------------------- */;@;	[ ;io:Sass::Tree::VariableNode:@guarded"!default:
@name"
black:
@expro:Sass::Script::Color	:@attrs{	:	bluei :redi :
alphai:
greeni ;0;@;i;@;	[ ;io;;"!default;"grayDarker;o;	;{	;i';i';i;i';0;@;i;@;	[ ;io;;"!default;"grayDark;o;	;{	;i8;i8;i;i8;0;@;i;@;	[ ;io;;"!default;"	gray;o;	;{	;iZ;iZ;i;iZ;0;@;i;@;	[ ;io;;"!default;"grayLight;o;	;{	;i�;i�;i;i�;0;@;i;@;	[ ;io;;"!default;"grayLighter;o;	;{	;i�;i�;i;i�;0;@;i;@;	[ ;io;;"!default;"
white;o;	;{	;i�;i�;i;i�;0;@;i;@;	[ ;io;

;;;["5/* Accent colors
 * ------------------------- */;@;	[ ;io;;"!default;"	blue;o;	;{	;i�;i	;i;i�;0;@;i;@;	[ ;io;;"!default;"blueDark;o;	;{	;i�;i ;i;ii;0;@;i;@;	[ ;io;;"!default;"
green;o;	;{	;iK;iK;i;i�;0;@;i;@;	[ ;io;;"!default;"red;o;	;{	;i";i�;i;i+;0;@;i;@;	[ ;io;;"!default;"yellow;o;	;{	;i;i�;i;i�;0;@;i ;@;	[ ;i o;;"!default;"orange;o;	;{	;i;i�;i;i�;0;@;i!;@;	[ ;i!o;;"!default;"	pink;o;	;{	;id;i�;i;i7;0;@;i";@;	[ ;i"o;;"!default;"purple;o;	;{	;i�;i;i;iH;0;@;i#;@;	[ ;i#o;

;;;["3/* Scaffolding
 * ------------------------- */;@;	[ ;i&o;;"!default;"bodyBackground;o:Sass::Script::Variable	;"
white;@;i(:@underscored_name"
white;@;	[ ;i(o;;"!default;"textColor;o;	;"grayDark;@;i);"grayDark;@;	[ ;i)o;

;;;["-/* Links
 * ------------------------- */;@;	[ ;i,o;;"!default;"linkColor;o;	;{	;i�;i ;i;i�;0;@;i.;@;	[ ;i.o;;"!default;"linkColorHover;o:Sass::Script::Funcall;"darken:@keywords{ ;@:@splat0;i/:
@args[o;	;"linkColor;@;i/;"linkColoro:Sass::Script::Number:@numerator_units["%:@original"15%;i;@:@denominator_units[ ;i/;@;	[ ;i/o;

;;;["2/* Typography
 * ------------------------- */;@;	[ ;i2o;;"!default;"sansFontFamily;o:Sass::Script::List	:@separator:
comma;[	o:Sass::Script::String	;:string;"Helvetica Neue;@;i4o;&	;:identifier;"Helvetica;@;i4o;&	;;(;"
Arial;@;i4o;&	;;(;"sans-serif;@;i4;@;i4;@;	[ ;i4o;;"!default;"serifFontFamily;o;#	;$;%;[	o;&	;;(;"Georgia;@;i5o;&	;;';"Times New Roman;@;i5o;&	;;(;"
Times;@;i5o;&	;;(;"
serif;@;i5;@;i5;@;	[ ;i5o;;"!default;"monoFontFamily;o;#	;$;%;[
o;&	;;(;"Monaco;@;i6o;&	;;(;"
Menlo;@;i6o;&	;;(;"Consolas;@;i6o;&	;;';"Courier New;@;i6o;&	;;(;"monospace;@;i6;@;i6;@;	[ ;i6o;;"!default;"baseFontSize;o;; ["px;!"	14px;i;@;"[ ;i8;@;	[ ;i8o;;"!default;"baseFontFamily;o;	;"sansFontFamily;@;i9;"sansFontFamily;@;	[ ;i9o;;"!default;"baseLineHeight;o;; ["px;!"	20px;i;@;"[ ;i:;@;	[ ;i:o;;"!default;"altFontFamily;o;	;"serifFontFamily;@;i;;"serifFontFamily;@;	[ ;i;o;;"!default;"headingsFontFamily;o;&	;;(;"inherit;@;i=;@;	[ ;i=o;

;;;["3/* empty to use BS default, $baseFontFamily */;@;	[ ;i=o;;"!default;"headingsFontWeight;o;&	;;(;"	bold;@;i>;@;	[ ;i>o;

;;;["+/* instead of browser default, bold */;@;	[ ;i>o;;"!default;"headingsColor;o;&	;;(;"inherit;@;i?;@;	[ ;i?o;

;;;["./* empty to use BS default, $textColor */;@;	[ ;i?o;

;;;["h/* Component sizing
 * -------------------------
 * Based on 14px font-size and 20px line-height */;@;	[ ;iBo;;"!default;"fontSizeLarge;o:Sass::Script::Operation
:@operand2o;; [ ;!"	1.25;f	1.25;@;"[ ;iF:@operator:
times:@operand1o;	;"baseFontSize;@;iF;"baseFontSize;@;iF;@;	[ ;iFo;

;;;["/* ~18px */;@;	[ ;iFo;;"!default;"fontSizeSmall;o;)
;*o;; [ ;!"	0.85;f0.84999999999999998 33;@;"@;iG;+;,;-o;	;"baseFontSize;@;iG;"baseFontSize;@;iG;@;	[ ;iGo;

;;;["/* ~12px */;@;	[ ;iGo;;"!default;"fontSizeMini;o;)
;*o;; [ ;!"	0.75;f	0.75;@;"@;iH;+;,;-o;	;"baseFontSize;@;iH;"baseFontSize;@;iH;@;	[ ;iHo;

;;;["/* ~11px */;@;	[ ;iHo;;"!default;"paddingLarge;o;#	;$:
space;[o;; ["px;!"	11px;i;@;"[ ;iJo;; ["px;!"	19px;i;@;"[ ;iJ;@;iJ;@;	[ ;iJo;

;;;["/* 44px */;@;	[ ;iJo;;"!default;"paddingSmall;o;#	;$;.;[o;; ["px;!"2px;i;@;"[ ;iKo;; ["px;!"	10px;i;@;"[ ;iK;@;iK;@;	[ ;iKo;

;;;["/* 26px */;@;	[ ;iKo;;"!default;"paddingMini;o;#	;$;.;[o;; ["px;!"0px;i ;@;"[ ;iLo;; ["px;!"6px;i;@;"[ ;iL;@;iL;@;	[ ;iLo;

;;;["/* 22px */;@;	[ ;iLo;;"!default;"baseBorderRadius;o;; ["px;!"4px;i	;@;"[ ;iN;@;	[ ;iNo;;"!default;"borderRadiusLarge;o;; ["px;!"6px;i;@;"[ ;iO;@;	[ ;iOo;;"!default;"borderRadiusSmall;o;; ["px;!"3px;i;@;"[ ;iP;@;	[ ;iPo;

;;;["./* Tables
 * ------------------------- */;@;	[ ;iSo;;"!default;"tableBackground;o;&	;;(;"transparent;@;iU;@;	[ ;iUo;

;;;["#/* overall background-color */;@;	[ ;iUo;;"!default;"tableBackgroundAccent;o;	;{	;i�;i�;i;i�;0;@;iV;@;	[ ;iVo;

;;;["/* for striping */;@;	[ ;iVo;;"!default;"tableBackgroundHover;o;	;{	;i�;i�;i;i�;0;@;iW;@;	[ ;iWo;

;;;["/* for hover */;@;	[ ;iWo;;"!default;"tableBorder;o;	;{	;i�;i�;i;i�;0;@;iX;@;	[ ;iXo;

;;;[" /* table and cell border */;@;	[ ;iXo;

;;;["//* Buttons
 * ------------------------- */;@;	[ ;iZo;;"!default;"btnBackground;o;	;"
white;@;i\;"
white;@;	[ ;i\o;;"!default;"btnBackgroundHighlight;o;;"darken;{ ;@;0;i];[o;	;"
white;@;i];"
whiteo;; ["%;!"10%;i;@;"[ ;i];@;	[ ;i]o;;"!default;"btnBorder;o;	;{	;i�;i�;i;i�;0;@;i^;@;	[ ;i^o;;"!default;"btnPrimaryBackground;o;	;"linkColor;@;i`;"linkColor;@;	[ ;i`o;;"!default;""btnPrimaryBackgroundHighlight;o;;"adjust-hue;{ ;@;0;ia;[o;	;"btnPrimaryBackground;@;ia;"btnPrimaryBackgroundo;; ["%;!"20%;i;@;"[ ;ia;@;	[ ;iao;;"!default;"btnInfoBackground;o;	;{	;i�;i`;i;i�;0;@;ic;@;	[ ;ico;;"!default;"btnInfoBackgroundHighlight;o;	;{	;i�;i4;i;i�;0;@;id;@;	[ ;ido;;"!default;"btnSuccessBackground;o;	;{	;ig;ig;i;i�;0;@;if;@;	[ ;ifo;;"!default;""btnSuccessBackgroundHighlight;o;	;{	;iV;iV;i;i�;0;@;ig;@;	[ ;igo;;"!default;"btnWarningBackground;o;;"lighten;{ ;@;0;ii;[o;	;"orange;@;ii;"orangeo;; ["%;!"15%;i;@;"[ ;ii;@;	[ ;iio;;"!default;""btnWarningBackgroundHighlight;o;	;"orange;@;ij;"orange;@;	[ ;ijo;;"!default;"btnDangerBackground;o;	;{	;i`;i�;i;id;0;@;il;@;	[ ;ilo;;"!default;"!btnDangerBackgroundHighlight;o;	;{	;i4;i�;i;i;;0;@;im;@;	[ ;imo;;"!default;"btnInverseBackground;o;	;{	;iI;iI;i;iI;0;@;io;@;	[ ;ioo;;"!default;""btnInverseBackgroundHighlight;o;	;"grayDarker;@;ip;"grayDarker;@;	[ ;ipo;

;;;["-/* Forms
 * ------------------------- */;@;	[ ;iso;;"!default;"inputBackground;o;	;"
white;@;iu;"
white;@;	[ ;iuo;;"!default;"inputBorder;o;	;{	;i�;i�;i;i�;0;@;iv;@;	[ ;ivo;;"!default;"inputBorderRadius;o;	;"baseBorderRadius;@;iw;"baseBorderRadius;@;	[ ;iwo;;"!default;"inputDisabledBackground;o;	;"grayLighter;@;ix;"grayLighter;@;	[ ;ixo;;"!default;"formActionsBackground;o;	;{	;i�;i�;i;i�;0;@;iy;@;	[ ;iyo;;0;"inputHeight;o;)
;*o;; ["px;!"	10px;i;@;"[ ;iz;+:	plus;-o;	;"baseLineHeight;@;iz;"baseLineHeight;@;iz;@;	[ ;izo;

;;;["J/* base line-height + 8px vertical padding + 2px top/bottom border */;@;	[ ;izo;

;;;["1/* Dropdowns
 * ------------------------- */;@;	[ ;i}o;;"!default;"dropdownBackground;o;	;"
white;@;i;"
white;@;	[ ;io;;"!default;"dropdownBorder;o;;"	rgba;{ ;@;0;i{;[	o;; [ ;!"0;i ;@;"@;i{o;; [ ;!"0;i ;@;"@;i{o;; [ ;!"0;i ;@;"@;i{o;; [ ;!"0.2;f0.20000000000000001 ��;@;"@;i{;@;	[ ;i{o;;"!default;"dropdownDividerTop;o;	;{	;i�;i�;i;i�;0;@;i|;@;	[ ;i|o;;"!default;"dropdownDividerBottom;o;	;"
white;@;i};"
white;@;	[ ;i}o;;"!default;"dropdownLinkColor;o;	;"grayDark;@;i;"grayDark;@;	[ ;io;;"!default;"dropdownLinkColorHover;o;	;"
white;@;i�;"
white;@;	[ ;i�o;;"!default;"dropdownLinkColorActive;o;	;"
white;@;i�;"
white;@;	[ ;i�o;;"!default;"!dropdownLinkBackgroundActive;o;	;"linkColor;@;i�;"linkColor;@;	[ ;i�o;;"!default;" dropdownLinkBackgroundHover;o;	;"!dropdownLinkBackgroundActive;@;i�;"!dropdownLinkBackgroundActive;@;	[ ;i�o;

;;;["T/* COMPONENT VARIABLES
 * -------------------------------------------------- */;@;	[ ;i�o;

;;;["�/* Z-index master list
 * -------------------------
 * Used for a bird's eye view of components dependent on the z-axis
 * Try to avoid customizing these :) */;@;	[ ;i�o;;"!default;"zindexDropdown;o;; [ ;!"	1000;i�;@;"@;i�;@;	[ ;i�o;;"!default;"zindexPopover;o;; [ ;!"	1010;i�;@;"@;i�;@;	[ ;i�o;;"!default;"zindexTooltip;o;; [ ;!"	1030;i;@;"@;i�;@;	[ ;i�o;;"!default;"zindexFixedNavbar;o;; [ ;!"	1030;i;@;"@;i�;@;	[ ;i�o;;"!default;"zindexModalBackdrop;o;; [ ;!"	1040;i;@;"@;i�;@;	[ ;i�o;;"!default;"zindexModal;o;; [ ;!"	1050;i;@;"@;i�;@;	[ ;i�o;

;;;["9/* Sprite icons path
 * ------------------------- */;@;	[ ;i�o;;"!default;"iconSpritePath;o;&	;;';"$../img/glyphicons-halflings.png;@;i�;@;	[ ;i�o;;"!default;"iconWhiteSpritePath;o;&	;;';"*../img/glyphicons-halflings-white.png;@;i�;@;	[ ;i�o;

;;;["D/* Input placeholder text color
 * ------------------------- */;@;	[ ;i�o;;"!default;"placeholderText;o;	;"grayLight;@;i�;"grayLight;@;	[ ;i�o;

;;;["7/* Hr border color
 * ------------------------- */;@;	[ ;i�o;;"!default;"hrBorder;o;	;"grayLighter;@;i�;"grayLighter;@;	[ ;i�o;

;;;["@/* Horizontal forms & lists
 * ------------------------- */;@;	[ ;i�o;;"!default;"horizontalComponentOffset;o;; ["px;!"
180px;i�;@;"[ ;i�;@;	[ ;i�o;

;;;["-/* Wells
 * ------------------------- */;@;	[ ;i�o;;"!default;"wellBackground;o;	;{	;i�;i�;i;i�;0;@;i�;@;	[ ;i�o;

;;;["./* Navbar
 * ------------------------- */;@;	[ ;i�o;;"!default;"navbarCollapseWidth;o;; ["px;!"
979px;i�;@;"[ ;i�;@;	[ ;i�o;;0;"navbarCollapseDesktopWidth;o;)
;*o;; [ ;!"1;i;@;"@;i�;+;/;-o;	;"navbarCollapseWidth;@;i�;"navbarCollapseWidth;@;i�;@;	[ ;i�o;;"!default;"navbarHeight;o;; ["px;!"	40px;i-;@;"[ ;i�;@;	[ ;i�o;;"!default;"navbarBackgroundHighlight;o;	;{	;i�;i�;i;i�;0;@;i�;@;	[ ;i�o;;"!default;"navbarBackground;o;;"darken;{ ;@;0;i�;[o;	;"navbarBackgroundHighlight;@;i�;"navbarBackgroundHighlighto;; ["%;!"5%;i
;@;"[ ;i�;@;	[ ;i�o;;"!default;"navbarBorder;o;;"darken;{ ;@;0;i�;[o;	;"navbarBackground;@;i�;"navbarBackgroundo;; ["%;!"12%;i;@;"[ ;i�;@;	[ ;i�o;;"!default;"navbarText;o;	;{	;i|;i|;i;i|;0;@;i�;@;	[ ;i�o;;"!default;"navbarLinkColor;o;	;{	;i|;i|;i;i|;0;@;i�;@;	[ ;i�o;;"!default;"navbarLinkColorHover;o;	;"grayDark;@;i�;"grayDark;@;	[ ;i�o;;"!default;"navbarLinkColorActive;o;	;"	gray;@;i�;"	gray;@;	[ ;i�o;;"!default;"navbarLinkBackgroundHover;o;&	;;(;"transparent;@;i�;@;	[ ;i�o;;"!default;"navbarLinkBackgroundActive;o;;"darken;{ ;@;0;i�;[o;	;"navbarBackground;@;i�;"navbarBackgroundo;; ["%;!"5%;i
;@;"[ ;i�;@;	[ ;i�o;;"!default;"navbarBrandColor;o;	;"navbarLinkColor;@;i�;"navbarLinkColor;@;	[ ;i�o;

;;;["/* Inverted navbar */;@;	[ ;i�o;;"!default;"navbarInverseBackground;o;	;{	;i;i;i;i;0;@;i�;@;	[ ;i�o;;"!default;"%navbarInverseBackgroundHighlight;o;	;{	;i';i';i;i';0;@;i�;@;	[ ;i�o;;"!default;"navbarInverseBorder;o;	;{	;i*;i*;i;i*;0;@;i�;@;	[ ;i�o;;"!default;"navbarInverseText;o;	;"grayLight;@;i�;"grayLight;@;	[ ;i�o;;"!default;"navbarInverseLinkColor;o;	;"grayLight;@;i�;"grayLight;@;	[ ;i�o;;"!default;" navbarInverseLinkColorHover;o;	;"
white;@;i�;"
white;@;	[ ;i�o;;"!default;"!navbarInverseLinkColorActive;o;	;" navbarInverseLinkColorHover;@;i�;" navbarInverseLinkColorHover;@;	[ ;i�o;;"!default;"%navbarInverseLinkBackgroundHover;o;&	;;(;"transparent;@;i�;@;	[ ;i�o;;"!default;"&navbarInverseLinkBackgroundActive;o;	;"navbarInverseBackground;@;i�;"navbarInverseBackground;@;	[ ;i�o;;"!default;""navbarInverseSearchBackground;o;;"lighten;{ ;@;0;i�;[o;	;"navbarInverseBackground;@;i�;"navbarInverseBackgroundo;; ["%;!"25%;i;@;"[ ;i�;@;	[ ;i�o;;"!default;"'navbarInverseSearchBackgroundFocus;o;	;"
white;@;i�;"
white;@;	[ ;i�o;;"!default;"navbarInverseSearchBorder;o;	;"navbarInverseBackground;@;i�;"navbarInverseBackground;@;	[ ;i�o;;"!default;"(navbarInverseSearchPlaceholderColor;o;	;{	;i�;i�;i;i�;0;@;i�;@;	[ ;i�o;;"!default;"navbarInverseBrandColor;o;	;"navbarInverseLinkColor;@;i�;"navbarInverseLinkColor;@;	[ ;i�o;

;;;["2/* Pagination
 * ------------------------- */;@;	[ ;i�o;;"!default;"paginationBackground;o;	;{	;i�;i�;i;i�;0;@;i�;@;	[ ;i�o;;"!default;"paginationBorder;o;	;{	;i�;i�;i;i�;0;@;i�;@;	[ ;i�o;;"!default;"paginationActiveBackground;o;	;{	;i�;i�;i;i�;0;@;i�;@;	[ ;i�o;

;;;["1/* Hero unit
 * ------------------------- */;@;	[ ;i�o;;"!default;"heroUnitBackground;o;	;"grayLighter;@;i�;"grayLighter;@;	[ ;i�o;;"!default;"heroUnitHeadingColor;o;&	;;(;"inherit;@;i�;@;	[ ;i�o;;"!default;"heroUnitLeadColor;o;&	;;(;"inherit;@;i�;@;	[ ;i�o;

;;;[">/* Form states and alerts
 * ------------------------- */;@;	[ ;i�o;;"!default;"warningText;o;	;{	;iX;i�;i;i�;0;@;i�;@;	[ ;i�o;;"!default;"warningBackground;o;	;{	;i�;i�;i;i�;0;@;i�;@;	[ ;i�o;;"!default;"warningBorder;o;;"darken;{ ;@;0;i�;[o;;"adjust-hue;{ ;@;0;i�;[o;	;"warningBackground;@;i�;"warningBackgroundo;; [ ;!"-10;i�;@;"@;i�o;; ["%;!"3%;i;@;"[ ;i�;@;	[ ;i�o;;"!default;"errorText;o;	;{	;iM;i�;i;iO;0;@;i�;@;	[ ;i�o;;"!default;"errorBackground;o;	;{	;i�;i�;i;i�;0;@;i�;@;	[ ;i�o;;"!default;"errorBorder;o;;"darken;{ ;@;0;i�;[o;;"adjust-hue;{ ;@;0;i�;[o;	;"errorBackground;@;i�;"errorBackgroundo;; [ ;!"-10;i�;@;"@;i�o;; ["%;!"3%;i;@;"[ ;i�;@;	[ ;i�o;;"!default;"successText;o;	;{	;iL;iK;i;i�;0;@;i�;@;	[ ;i�o;;"!default;"successBackground;o;	;{	;i�;i�;i;i�;0;@;i�;@;	[ ;i�o;;"!default;"successBorder;o;;"darken;{ ;@;0;i�;[o;;"adjust-hue;{ ;@;0;i�;[o;	;"successBackground;@;i�;"successBackgroundo;; [ ;!"-10;i�;@;"@;i�o;; ["%;!"5%;i
;@;"[ ;i�;@;	[ ;i�o;;"!default;"infoText;o;	;{	;i�;i?;i;i�;0;@;i�;@;	[ ;i�o;;"!default;"infoBackground;o;	;{	;i�;i�;i;i�;0;@;i�;@;	[ ;i�o;;"!default;"infoBorder;o;;"darken;{ ;@;0;i�;[o;;"adjust-hue;{ ;@;0;i�;[o;	;"infoBackground;@;i�;"infoBackgroundo;; [ ;!"-10;i�;@;"@;i�o;; ["%;!"7%;i;@;"[ ;i�;@;	[ ;i�o;

;;;["=/* Tooltips and popovers
 * ------------------------- */;@;	[ ;i�o;;"!default;"tooltipColor;o;	;{	;i�;i�;i;i�;0;@;i�;@;	[ ;i�o;;"!default;"tooltipBackground;o;	;{	;i ;i ;i;i ;0;@;i�;@;	[ ;i�o;;"!default;"tooltipArrowWidth;o;; ["px;!"5px;i
;@;"[ ;i�;@;	[ ;i�o;;"!default;"tooltipArrowColor;o;	;"tooltipBackground;@;i�;"tooltipBackground;@;	[ ;i�o;;"!default;"popoverBackground;o;	;{	;i�;i�;i;i�;0;@;i;@;	[ ;io;;"!default;"popoverArrowWidth;o;; ["px;!"	10px;i;@;"[ ;i;@;	[ ;io;;"!default;"popoverArrowColor;o;	;{	;i�;i�;i;i�;0;@;i;@;	[ ;io;;"!default;"popoverTitleBackground;o;;"darken;{ ;@;0;i;[o;	;"popoverBackground;@;i;"popoverBackgroundo;; ["%;!"3%;i;@;"[ ;i;@;	[ ;io;

;;;["+/* Special enhancement for popovers */;@;	[ ;io;;"!default;"popoverArrowOuterWidth;o;)
;*o;; [ ;!"1;i;@;"@;i;+;/;-o;	;"popoverArrowWidth;@;i;"popoverArrowWidth;@;i;@;	[ ;io;;"!default;"popoverArrowOuterColor;o;;"	rgba;{ ;@;0;i;[	o;; [ ;!"0;i ;@;"@;io;; [ ;!"0;i ;@;"@;io;; [ ;!"0;i ;@;"@;io;; [ ;!"	0.25;f	0.25;@;"@;i;@;	[ ;io;

;;;["E/* GRID
 * -------------------------------------------------- */;@;	[ ;io;

;;;[":/* Default 940px grid
 * ------------------------- */;@;	[ ;io;;"!default;"gridColumns;o;; [ ;!"12;i;@;"@;i;@;	[ ;io;;"!default;"gridColumnWidth;o;; ["px;!"	60px;iA;@;"[ ;i;@;	[ ;io;;"!default;"gridGutterWidth;o;; ["px;!"	20px;i;@;"[ ;i;@;	[ ;io;;"!default;"gridRowWidth;o;)
;*o;)
;*o;)
;*o;; [ ;!"1;i;@;"@;i;+:
minus;-o;	;"gridColumns;@;i;"gridColumns;@;i;+;,;-o;	;"gridGutterWidth;@;i;"gridGutterWidth;@;i;+;/;-o;)
;*o;	;"gridColumnWidth;@;i;"gridColumnWidth;+;,;-o;	;"gridColumns;@;i;"gridColumns;@;i;@;i;@;	[ ;io;

;;;["/* 1200px min */;@;	[ ;io;;"!default;"gridColumnWidth1200;o;; ["px;!"	70px;iK;@;"[ ;i;@;	[ ;io;;"!default;"gridGutterWidth1200;o;; ["px;!"	30px;i#;@;"[ ;i;@;	[ ;io;;"!default;"gridRowWidth1200;o;)
;*o;)
;*o;)
;*o;; [ ;!"1;i;@;"@;i;+;0;-o;	;"gridColumns;@;i;"gridColumns;@;i;+;,;-o;	;"gridGutterWidth1200;@;i;"gridGutterWidth1200;@;i;+;/;-o;)
;*o;	;"gridColumnWidth1200;@;i;"gridColumnWidth1200;+;,;-o;	;"gridColumns;@;i;"gridColumns;@;i;@;i;@;	[ ;io;

;;;["/* 768px-979px */;@;	[ ;io;;"!default;"gridColumnWidth768;o;; ["px;!"	42px;i/;@;"[ ;i;@;	[ ;io;;"!default;"gridGutterWidth768;o;; ["px;!"	20px;i;@;"[ ;i;@;	[ ;io;;"!default;"gridRowWidth768;o;)
;*o;)
;*o;)
;*o;; [ ;!"1;i;@;"@;i;+;0;-o;	;"gridColumns;@;i;"gridColumns;@;i;+;,;-o;	;"gridGutterWidth768;@;i;"gridGutterWidth768;@;i;+;/;-o;)
;*o;	;"gridColumnWidth768;@;i;"gridColumnWidth768;+;,;-o;	;"gridColumns;@;i;"gridColumns;@;i;@;i;@;	[ ;io;

;;;["2/* Fluid grid
 * ------------------------- */;@;	[ ;i"o;;"!default;"fluidGridColumnWidth;o;;"percentage;{ ;@;0;i$;[o;)
;*o;	;"gridRowWidth;@;i$;"gridRowWidth;+:div;-o;	;"gridColumnWidth;@;i$;"gridColumnWidth;@;i$;@;	[ ;i$o;;"!default;"fluidGridGutterWidth;o;;"percentage;{ ;@;0;i%;[o;)
;*o;	;"gridRowWidth;@;i%;"gridRowWidth;+;1;-o;	;"gridGutterWidth;@;i%;"gridGutterWidth;@;i%;@;	[ ;i%o;

;;;["/* 1200px min */;@;	[ ;i'o;;"!default;"fluidGridColumnWidth1200;o;;"percentage;{ ;@;0;i(;[o;)
;*o;	;"gridRowWidth1200;@;i(;"gridRowWidth1200;+;1;-o;	;"gridColumnWidth1200;@;i(;"gridColumnWidth1200;@;i(;@;	[ ;i(o;;"!default;"fluidGridGutterWidth1200;o;;"percentage;{ ;@;0;i);[o;)
;*o;	;"gridRowWidth1200;@;i);"gridRowWidth1200;+;1;-o;	;"gridGutterWidth1200;@;i);"gridGutterWidth1200;@;i);@;	[ ;i)o;

;;;["/* 768px-979px */;@;	[ ;i+o;;"!default;"fluidGridColumnWidth768;o;;"percentage;{ ;@;0;i,;[o;)
;*o;	;"gridRowWidth768;@;i,;"gridRowWidth768;+;1;-o;	;"gridColumnWidth768;@;i,;"gridColumnWidth768;@;i,;@;	[ ;i,o;;"!default;"fluidGridGutterWidth768;o;;"percentage;{ ;@;0;i-;[o;)
;*o;	;"gridRowWidth768;@;i-;"gridRowWidth768;+;1;-o;	;"gridGutterWidth768;@;i-;"gridGutterWidth768;@;i-;@;	[ ;i-;i