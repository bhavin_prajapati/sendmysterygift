/*
 * server/controllers/api/top.js
 */

'use strict';

//var fs = require('fs'),
//    path = require('path');
//var http = require('http');

/*IMPORTANT: Make sure you fill in your secret API key into the stripe module.*/
var stripe = require('stripe')('sk_test_etBIlRYSPlM7OPHXuy1irChq');
var nodemailer = require('nodemailer');

function LIST(req, cb) {
  cb('invalid method - ' + req.method);
}

function GET(req, id, cb) {
  cb('invalid method - ' + req.method);
}


function sendEmail( options ){
  // Create a SMTP transport object
  var transport = nodemailer.createTransport('SMTP', {
    service: 'Gmail', // use well known service.
                        // If you are using @gmail.com address, then you don't
                        // even have to define the service name
    auth: {
        user: 'support@sendmysterygift.com',
        pass: 'Welcome2014!!'
      }
    });

  console.log('SMTP Configured');

  // Message object
  var message = {

      // sender info
      from: '"Mystery Gift" <support@sendmysterygift.com>',

      // Comma separated list of recipients
      to: '"' + options.firstname + ' ' + options.lastname + '" <' + options.email + '>',

      // Subject of the message
      subject: options.subject, //

      headers: {
          'X-Laziness-level': 1000
        },

        // plaintext body
        //text: 'Hello to myself!',

        // HTML body
        html: options.html,

      };

  console.log('Sending Mail');
  transport.sendMail(message, function(error){
      if(error){
        console.log('Error occured');
        console.log(error.message);
        return;
      }
      console.log('Message sent successfully!');

      // if you don't want to use this transport object anymore, uncomment following line
      //transport.close(); // close the connection pool
    });
}

function POST(request, cb) {
  //Object POSTed to server contains the Stripe token from Stripe's servers.
  var transaction = request.body;
  var stripeToken = transaction.stripeToken;
  var amount = transaction.amount;
  var fullname = transaction.fullname;
  var email = transaction.email;
  var firstname = transaction.firstname;
  var lastname = transaction.lastname;
  var sex = transaction.sex;
  var age = transaction.age;
  var address1 = transaction.address1;
  var address2 = transaction.address2;
  var city = transaction.city;
  var stateprovince = transaction.stateprovince;
  var country = transaction.country;
  var zipcodepostalcode = transaction.zipcodepostalcode;
  var phonenumber = transaction.phonenumber;
  var message = transaction.message;

  var clothingandapparel = ' ';
  if (transaction.clothingandapparel) { clothingandapparel = '[clothingandapparel]'; }
  var artsandcrafts = ' ';
  if (transaction.artsandcrafts) { artsandcrafts = '[artsandcrafts]'; }
  var batteriesandchargers = ' ';
  if (transaction.batteriesandchargers) { batteriesandchargers = '[batteriesandchargers]'; }
  var books = ' ';
  if (transaction.books) { books = '[books]'; }
  var candles = ' ';
  if (transaction.candles) { candles = '[candles]'; }
  var consumerelectronics = ' ';
  if (transaction.consumerelectronics) { consumerelectronics = '[consumerelectronics]'; }
  var hats = ' ';
  if (transaction.hats) { hats = '[hats]'; }
  var healthandbeautysupplies = ' ';
  if (transaction.healthandbeautysupplies) { healthandbeautysupplies = '[healthandbeautysupplies]'; }
  var homedecor = ' ';
  if (transaction.homedecor) { homedecor = '[homedecor]'; }
  var artificialjewellery = ' ';
  if (transaction.artificialjewellery) { artificialjewellery = '[artificialjewellery]'; }
  var magazinesandcatalogues = ' ';
  if (transaction.magazinesandcatalogues) { magazinesandcatalogues = '[magazinesandcatalogues]'; }
  var officesupplies = ' ';
  if (transaction.officesupplies) { officesupplies = '[officesupplies]'; }
  var toysgamesandhobby = ' ';
  if (transaction.toysgamesandhobby) { toysgamesandhobby = '[toysgamesandhobby]'; }
  var cosmetics = ' ';
  if (transaction.cosmetics) { cosmetics = '[cosmetics]'; }
  var other = ' ';
  if (transaction.other) { other = '[other]'; }

  var categories = clothingandapparel + artsandcrafts + batteriesandchargers + books + candles + consumerelectronics + hats + healthandbeautysupplies + homedecor + artificialjewellery + magazinesandcatalogues + officesupplies + toysgamesandhobby + cosmetics + other;

  var orderNumber = Math.floor(Math.random() * 1000000000);

  //For now, charge $10.00 to the customer.
  var charge =
  {
    amount: amount*100, //Charge is in cents
    currency: 'USD',
    card: stripeToken //Card can either be a Stripe token, or an object containing credit card properties.
  };

  stripe.charges.create(charge,
      //All stripe module functions take a callback, consisting of two params:
      // the error, then the response.
      function(err, charge)
      {
        if(err)
        {
          console.log(err);
        }
        else
        {
          //res.json(charge);
          var res = cb(null,'getres');

          if(charge.paid) {
            console.log('charge.paid = true');
            sendEmail({
              firstname: fullname.split(' ')[0],
              lastname: fullname.split(' ')[1],
              email: email,
              subject: 'Mystery Gift Order #' + orderNumber,
              html: '<h2>Mystery Gift Order #' + orderNumber + '</h2><p>Your order from Mystery Gift has been successfully completed. Please give approximately 10 - 15 business days for the order to arrive.</p>'
            });

            sendEmail({
              firstname: 'Mystery',
              lastname: 'Gift',
              email: 'support@sendmysterygift.com',
              subject: 'Order #' + orderNumber,
              html: '<p>Recepient Information</p><p>amount: ' + amount + '</p><p>firstname: ' + firstname + '</p><p>lastname: ' + lastname + '</p><p>sex: ' + sex + '</p><p>age: ' + age + '</p><p>address1: ' + address1 + '</p><p>address2: ' + address2 + '</p><p>city: ' + city + '</p><p>state_province: ' + stateprovince + '</p><p>country: ' + country + '</p><p>zipcode_postalcode: ' + zipcodepostalcode + '</p><p>phonenumber: ' + phonenumber + '</p><p>message: ' + message + '</p><p>categories: ' + categories + '</p>'
            });
            res.render('purchase/index', {
              confirm: 'true'
            });
          } else {
            console.log('charge.paid = false');
            res.render('purchase/index', {
              confirm: 'false'
            });
          }

          //cb(null, charge, {'Content-Type': 'application/json'});

          console.log('Successful charge sent to Stripe!');
        }
      }
  );
}

function PUT(req, id, cb) {
  cb('invalid method - ' + req.method);
}

function DELETE(req, id, cb) {
  cb('invalid method - ' + req.method);
}

// Public API
exports.__filename = __filename;
exports.LIST = LIST;
exports.GET = GET;
exports.POST = POST;
exports.PUT = PUT;
exports.DELETE = DELETE;
