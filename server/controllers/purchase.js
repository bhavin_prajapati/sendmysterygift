/*
 * server/controllers/purchase.js
 */

'use strict';

function index(req, res) {
  res.render('purchase/index');
}

// Public API
exports.index = index;
